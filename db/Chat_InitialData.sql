INSERT INTO `chat`.`category` (`name`) VALUES ('Automotive');
INSERT INTO `chat`.`category` (`name`) VALUES ('Computer engineering');
INSERT INTO `chat`.`category` (`name`) VALUES ('Free time');
INSERT INTO `chat`.`category` (`name`) VALUES ('Jokes');
INSERT INTO `chat`.`category` (`name`) VALUES ('Politics');

INSERT INTO `chat`.`interest` (`name`) VALUES ('Programming');
INSERT INTO `chat`.`interest` (`name`) VALUES ('Cycling');
INSERT INTO `chat`.`interest` (`name`) VALUES ('Running');
INSERT INTO `chat`.`interest` (`name`) VALUES ('Diving');
INSERT INTO `chat`.`interest` (`name`) VALUES ('Dancing');
INSERT INTO `chat`.`interest` (`name`) VALUES ('Walking');
