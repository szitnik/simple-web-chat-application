-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema chat
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `chat` ;

-- -----------------------------------------------------
-- Schema chat
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `chat` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ;
USE `chat` ;

-- -----------------------------------------------------
-- Table `chat`.`category`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `chat`.`category` ;

CREATE TABLE IF NOT EXISTS `chat`.`category` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `name` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '')
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `chat`.`user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `chat`.`user` ;

CREATE TABLE IF NOT EXISTS `chat`.`user` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `firstName` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT '',
  `lastName` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT '',
  `username` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT '',
  `password` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT '',
  `image` BLOB NULL DEFAULT NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '',
  UNIQUE INDEX `username_UNIQUE` (`username` ASC)  COMMENT '')
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `chat`.`interest`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `chat`.`interest` ;

CREATE TABLE IF NOT EXISTS `chat`.`interest` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `name` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '')
ENGINE = InnoDB
AUTO_INCREMENT = 8
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `chat`.`hasinterest`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `chat`.`hasinterest` ;

CREATE TABLE IF NOT EXISTS `chat`.`hasinterest` (
  `idInterest` INT(11) NOT NULL COMMENT '',
  `idUser` INT(11) NOT NULL COMMENT '',
  PRIMARY KEY (`idInterest`, `idUser`)  COMMENT '',
  INDEX `fk1_idx` (`idUser` ASC)  COMMENT '',
  CONSTRAINT `fk1`
    FOREIGN KEY (`idUser`)
    REFERENCES `chat`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk2`
    FOREIGN KEY (`idInterest`)
    REFERENCES `chat`.`interest` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `chat`.`privatemessage`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `chat`.`privatemessage` ;

CREATE TABLE IF NOT EXISTS `chat`.`privatemessage` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `time` DATETIME NOT NULL COMMENT '',
  `content` VARCHAR(500) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT '',
  `fromUser` INT(11) NOT NULL COMMENT '',
  `toUser` INT(11) NOT NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '',
  INDEX `fk6_idx` (`fromUser` ASC)  COMMENT '',
  INDEX `fk7_idx` (`toUser` ASC)  COMMENT '',
  CONSTRAINT `fk6`
    FOREIGN KEY (`fromUser`)
    REFERENCES `chat`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk7`
    FOREIGN KEY (`toUser`)
    REFERENCES `chat`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `chat`.`publicmessage`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `chat`.`publicmessage` ;

CREATE TABLE IF NOT EXISTS `chat`.`publicmessage` (
  `id` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `time` DATETIME NOT NULL COMMENT '',
  `content` VARCHAR(500) CHARACTER SET 'utf8' COLLATE 'utf8_unicode_ci' NOT NULL COMMENT '',
  `idUser` INT(11) NOT NULL COMMENT '',
  `idCategory` INT(11) NOT NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '',
  INDEX `fk4_idx` (`idCategory` ASC)  COMMENT '',
  INDEX `fk5_idx` (`idUser` ASC)  COMMENT '',
  CONSTRAINT `fk4`
    FOREIGN KEY (`idCategory`)
    REFERENCES `chat`.`category` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk5`
    FOREIGN KEY (`idUser`)
    REFERENCES `chat`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

SET SQL_MODE = '';
GRANT USAGE ON *.* TO chat;
 DROP USER chat;
SET SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';
CREATE USER 'chat' IDENTIFIED BY 'BeReSeMi.01';

GRANT ALL ON `mydb`.* TO 'chat';
GRANT ALL ON `chat`.* TO 'chat';

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
