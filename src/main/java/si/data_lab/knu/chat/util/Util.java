package si.data_lab.knu.chat.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import si.data_lab.knu.chat.controller.Controller;
import spark.Request;

import java.io.IOException;
import java.io.StringWriter;

/**
 * @author slavkoz
 */
public class Util {

    public static String dataToJson(Object data) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.enable(SerializationFeature.INDENT_OUTPUT);
            StringWriter sw = new StringWriter();
            mapper.writeValue(sw, data);
            return sw.toString();
        } catch (IOException e){
            throw new RuntimeException("IOException from a StringWriter?");
        }
    }

    public static boolean loggedIn(Request request) {
        Object loggedObject = request.session().attribute(Controller.SESSION_USERNAME);
        if (loggedObject != null && loggedObject.toString().length() > 0) {
            return true;
        } else {
            return false;
        }
    }
}
