package si.data_lab.knu.chat.util;

import si.data_lab.knu.chat.db.DBManager;

/**
 * Created by slavkoz on 18/07/15.
 */
public class DBKeepAwakeRunner implements Runnable {
    @Override
    public void run() {
        while (true) {
            DBManager.getAllUsers();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
