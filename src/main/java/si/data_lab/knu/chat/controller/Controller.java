package si.data_lab.knu.chat.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import si.data_lab.knu.chat.db.DBManager;
import si.data_lab.knu.chat.model.Interest;
import si.data_lab.knu.chat.model.PrivateMessage;
import si.data_lab.knu.chat.model.PublicMessage;
import si.data_lab.knu.chat.model.User;
import spark.ModelAndView;
import spark.Session;
import spark.template.handlebars.HandlebarsTemplateEngine;

import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletInputStream;
import javax.servlet.http.Part;
import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.HashMap;

import static spark.Spark.*;
import static si.data_lab.knu.chat.util.Util.*;

/**
 * @author slavkoz
 */
public class Controller {
    public static final int HTTP_BAD_REQUEST = 400;
    public static final String SESSION_USERNAME = "username";

    public static void setEndpoints() {

        get("/", (request, response) -> {
            HashMap map = new HashMap();
            return new ModelAndView(map, "index.html");
        }, new HandlebarsTemplateEngine());

        post("/login", (request, response) -> {
            HashMap map = new HashMap();

            try {
                User user = new User();
                user.setUsername(request.queryMap("username").value());
                user.setPassword(request.queryMap("password").value());

                boolean loginOK = DBManager.checkUsernameAndPassword(user);

                user = DBManager.getUser(user.getUsername());

                if (loginOK) {
                    Session session = request.session(true);
                    session.attribute(SESSION_USERNAME, user.getUsername());
                    response.redirect("/chat");
                    return null;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            map.put("msg", "Wrong username or password!");
            return new ModelAndView(map, "index.html");
        }, new HandlebarsTemplateEngine());

        post("/register", (request, response) -> {
            HashMap map = new HashMap();

            try {
                User user = new User();
                user.setFirstName(request.queryMap("firstName").value());
                user.setLastName(request.queryMap("lastName").value());
                user.setUsername(request.queryMap("username").value());
                user.setPassword(request.queryMap("password").value());


                boolean registerOK = DBManager.saveUser(user);

                if (registerOK) {
                    Session session = request.session(true);
                    session.attribute(SESSION_USERNAME, user.getUsername());
                    response.redirect("/chat");
                    return null;
                }
            } catch (Exception e) { e.printStackTrace(); }

            map.put("msg", "<div class=\"alert alert-danger\" role=\"alert\" style=\"position: relative; top: -600px; width: 750px;\">Error happened or user already exists!</div>");
            return new ModelAndView(map, "index.html");
        }, new HandlebarsTemplateEngine());

        get("/logout", (request, response) -> {
            request.session().attribute(SESSION_USERNAME, null);
            response.redirect("/");
            return "";
        });

        get("/chat", (request, response) -> {
            HashMap map = new HashMap();

            if (!loggedIn(request)) {
                response.status(HTTP_BAD_REQUEST);
                map.put("msg", "<div class=\"alert alert-danger\" role=\"alert\" style=\"position: relative; top: -600px; width: 750px;\">Error happened or user already exists!</div>");
                return new ModelAndView(map, "index.html");
            }
            String username = request.session().attribute(Controller.SESSION_USERNAME);

            User user = DBManager.getUser(username);

            map.put("username", user.getUsername());
            map.put("name", user.getFirstName() + " " + user.getLastName());
            return new ModelAndView(map, "chat.html");
        }, new HandlebarsTemplateEngine());


        post("/publicMessage", (request, response) -> {
            if (!loggedIn(request)) {
                response.status(HTTP_BAD_REQUEST);
                return "";
            }
            String username = request.session().attribute(Controller.SESSION_USERNAME);

            ObjectMapper mapper = new ObjectMapper();
            PublicMessage message = mapper.readValue(request.body(), PublicMessage.class);

            message.setTime(GregorianCalendar.getInstance().getTime());
            message.setIdUser(DBManager.getUserId(username));

            DBManager.saveMessage(message);

            response.status(200);
            response.type("application/json");
            return dataToJson(Arrays.asList(new PublicMessage[]{message}));
        });

        get("/publicMessages/:idCategory", (request, response) -> {
            if (!loggedIn(request)) {
                response.status(HTTP_BAD_REQUEST);
                return "";
            }
            response.status(200);
            response.type("application/json");
            return dataToJson(DBManager.getAllPublicMessages(Integer.parseInt(request.params(":idCategory"))));
        });

        get("/publicMessages/:idCategory/:id", (request, response) -> {
            if (!loggedIn(request)) {
                response.status(HTTP_BAD_REQUEST);
                return "";
            }
            response.status(200);
            response.type("application/json");
            return dataToJson(DBManager.getAllPublicMessagesAfterId(
                    Integer.parseInt(request.params(":idCategory")),
                    Integer.parseInt(request.params(":id"))));
        });

        get("/users", (request, response) -> {
            if (!loggedIn(request)) {
                response.status(HTTP_BAD_REQUEST);
                return "";
            }
            response.status(200);
            response.type("application/json");
            return dataToJson(DBManager.getAllUsers());
        });

        get("/users/:id", (request, response) -> {
            if (!loggedIn(request)) {
                response.status(HTTP_BAD_REQUEST);
                return "";
            }
            response.status(200);
            response.type("application/json");
            return dataToJson(DBManager.getAllUsersAfterId(
                    Integer.parseInt(request.params(":id"))));
        });

        get("/user/:id", (request, response) -> {
            if (!loggedIn(request)) {
                response.status(HTTP_BAD_REQUEST);
                return "";
            }
            response.status(200);
            response.type("application/json");
            return dataToJson(DBManager.getUser(
                    Integer.parseInt(request.params(":id"))));
        });

        get("/loggedUserId", (request, response) -> {
            if (!loggedIn(request)) {
                response.status(HTTP_BAD_REQUEST);
                return "";
            }
            response.status(200);
            response.type("application/json");
            String username = request.session().attribute(Controller.SESSION_USERNAME);
            return dataToJson(DBManager.getUserId(username));
        });

        get("/categories", (request, response) -> {
            if (!loggedIn(request)) {
                response.status(HTTP_BAD_REQUEST);
                return "";
            }
            response.status(200);
            response.type("application/json");
            return dataToJson(DBManager.getAllCategories());
        });

        post("/privateMessage", (request, response) -> {
            if (!loggedIn(request)) {
                response.status(HTTP_BAD_REQUEST);
                return "";
            }
            String username = request.session().attribute(Controller.SESSION_USERNAME);

            ObjectMapper mapper = new ObjectMapper();
            PrivateMessage message = mapper.readValue(request.body(), PrivateMessage.class);

            message.setTime(GregorianCalendar.getInstance().getTime());
            message.setFromUser(DBManager.getUserId(username));

            DBManager.saveMessage(message);

            response.status(200);
            response.type("application/json");
            return dataToJson(Arrays.asList(new PrivateMessage[]{message}));
        });

        get("/privateMessages/:id", (request, response) -> {
            if (!loggedIn(request)) {
                response.status(HTTP_BAD_REQUEST);
                return "";
            }
            String username = request.session().attribute(Controller.SESSION_USERNAME);
            response.status(200);
            response.type("application/json");

            return dataToJson(DBManager.getAllPrivateMessagesAfterId(
                    DBManager.getUserId(username),
                    Integer.parseInt(request.params(":id"))));
        });

        get("/interests", (request, response) -> {
            if (!loggedIn(request)) {
                response.status(HTTP_BAD_REQUEST);
                return "";
            }
            response.status(200);
            response.type("application/json");
            return dataToJson(DBManager.getAllInterests());
        });

        post("/updateInterest/:id/:checked", (request, response) -> {
            if (!loggedIn(request)) {
                response.status(HTTP_BAD_REQUEST);
                return "";
            }
            String username = request.session().attribute(Controller.SESSION_USERNAME);
            int idUser = DBManager.getUserId(username);

            boolean checked = Boolean.parseBoolean(request.params(":checked"));
            int idInterest = Integer.parseInt(request.params(":id"));

            if (checked) {
                DBManager.setInterest(idUser, idInterest);
            } else {
                DBManager.deleteInterest(idUser, idInterest);
            }

            response.status(200);
            response.type("application/json");
            return "";
        });

        get("/interest/:id", (request, response) -> {
            if (!loggedIn(request)) {
                response.status(HTTP_BAD_REQUEST);
                return "";
            }
            String username = request.session().attribute(Controller.SESSION_USERNAME);
            int idUser = DBManager.getUserId(username);

            int idInterest = Integer.parseInt(request.params(":id"));

            boolean retVal = DBManager.hasInterest(idUser, idInterest);

            response.status(200);
            response.type("application/json");
            return retVal + "";
        });

        get("/profileImage", (request, response) -> {
            if (!loggedIn(request)) {
                response.status(HTTP_BAD_REQUEST);
                return "";
            }

            String username = request.session().attribute(Controller.SESSION_USERNAME);
            int idUser = DBManager.getUserId(username);

            byte[] image = DBManager.getUser(idUser).getImage();
            if (image == null) {
                response.redirect("/images/blank-profile.jpg");
            }

            response.status(200);
            response.type("image");
            return image;
        });

        post("/profileImage", (request, response) -> {
            if (!loggedIn(request)) {
                response.status(HTTP_BAD_REQUEST);
                return "";
            }

            String username = request.session().attribute(Controller.SESSION_USERNAME);
            int idUser = DBManager.getUserId(username);

            MultipartConfigElement multipartConfigElement = new MultipartConfigElement("/tmp");
            request.raw().setAttribute("org.eclipse.multipartConfig", multipartConfigElement);


            Part file = request.raw().getPart("file"); //file is name of the upload form

            byte[] image = new byte[(int)file.getSize()];
            file.getInputStream().read(image);

            DBManager.updateProfileImage(idUser, image);

            response.redirect("/chat");
            return null;
        });
    }
}
