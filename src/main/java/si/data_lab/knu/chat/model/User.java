package si.data_lab.knu.chat.model;

import lombok.Data;

/**
 * @author slavkoz
 */
@Data
public class User {
    private int id;
    private String firstName;
    private String lastName;
    private String username;
    private String password;
    private byte[] image;
}
