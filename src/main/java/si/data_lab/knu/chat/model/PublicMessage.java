package si.data_lab.knu.chat.model;

import lombok.Data;

import java.util.Date;

/**
 * @author slavkoz
 */
@Data
public class PublicMessage {
    private int id;
    private Date time;
    private String content;
    private int idUser;
    private int idCategory;
}
