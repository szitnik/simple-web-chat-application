package si.data_lab.knu.chat.model;

import lombok.Data;

import java.util.Date;

/**
 * @author slavkoz
 */
@Data
public class PrivateMessage {
    private int id;
    private Date time;
    private String content;
    private int fromUser;
    private int toUser;
}
