package si.data_lab.knu.chat.model;

import lombok.Data;

/**
 * @author slavkoz
 */
@Data
public class HasInterest {
    private int idInterest;
    private int idUser;
}
