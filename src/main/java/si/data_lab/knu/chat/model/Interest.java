package si.data_lab.knu.chat.model;

import lombok.Data;

/**
 * @author slavkoz
 */
@Data
public class Interest {
    private int id;
    private String name;
}
