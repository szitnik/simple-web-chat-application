package si.data_lab.knu.chat;

import si.data_lab.knu.chat.controller.Controller;
import si.data_lab.knu.chat.util.DBKeepAwakeRunner;

import java.awt.*;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import static spark.Spark.*;

/**
 * @author slavkoz
 */
public class Chat {

    public static void main(String[] args) throws URISyntaxException, IOException {

        //Parameters definition
        int port = 8080;

        //Setup global settings
        port(port);
        staticFileLocation("/static");


        //Set endpoints
        Controller.setEndpoints();


        //Open default URL
        Desktop.getDesktop().browse(new URI("http://localhost:8080"));

        //Keep the connection to database open
        new DBKeepAwakeRunner().run();
    }

}
