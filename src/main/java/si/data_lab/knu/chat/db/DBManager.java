package si.data_lab.knu.chat.db;

import org.joda.time.DateTime;
import org.sql2o.Connection;
import org.sql2o.QuirksMode;
import org.sql2o.Sql2o;
import si.data_lab.knu.chat.model.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author slavkoz
 */
public class DBManager {
    private static final String DB_URL = "jdbc:mysql://localhost/chat";
    private static final String USER = "chat";
    private static final String PASS = "BeReSeMi.01";
    private static Sql2o sql2o;

    static {
        sql2o = new Sql2o(DB_URL, USER, PASS);
    }

    /**
     * Insert attributes firstName, lastName, username and password into a database.
     * @param user
     * @return
     */
    public static boolean saveUser(User user) {
        int rowsUpdated = 0;
        String sql = "INSERT INTO User (firstName, lastName, username, password) VALUES (:firstName, :lastName, :username, :password)";
        try (Connection con = sql2o.open()) {
            con.createQuery(sql)
                    .addParameter("firstName", user.getFirstName())
                    .addParameter("lastName", user.getLastName())
                    .addParameter("username", user.getUsername())
                    .addParameter("password", user.getPassword())
                    .executeUpdate();
            rowsUpdated = con.getResult();
        }

        if (rowsUpdated > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check, whether a user, identified by username and password, exists.
     * @param user
     * @return
     */
    public static Boolean checkUsernameAndPassword(User user) {
        boolean retVal = false;
        String sql = "SELECT * FROM User WHERE username = :username AND password = :password";

        try(Connection con = sql2o.open()) {
            List<User> users = con.createQuery(sql)
                    .addParameter("username", user.getUsername())
                    .addParameter("password", user.getPassword())
                    .executeAndFetch(User.class);
            if (users.size() > 0) retVal = true;
        }

        return retVal;
    }

    public static List<PublicMessage> getAllPublicMessages(int idCategory) {
        List<PublicMessage> retVal = new ArrayList<>();
        String sql = "SELECT * FROM PublicMessage WHERE idCategory = :idCategory";

        try(Connection con = sql2o.open()) {
            retVal = con.createQuery(sql)
                    .addParameter("idCategory", idCategory)
                    .executeAndFetch(PublicMessage.class);
        }

        return retVal;
    }

    /**
     * Return all public messages, which have id greater than the provided value.
     * @param idCategory
     * @param id
     * @return
     */
    public static List<PublicMessage> getAllPublicMessagesAfterId(int idCategory, int id) {
        List<PublicMessage> retVal = new ArrayList<>();
        String sql = "SELECT * FROM PublicMessage WHERE id > :id AND idCategory = :idCategory";

        try(Connection con = sql2o.open()) {
            retVal = con.createQuery(sql)
                    .addParameter("id", id)
                    .addParameter("idCategory", idCategory)
                    .executeAndFetch(PublicMessage.class);
        }

        return retVal;
    }

    public static int getUserId(String username) {
        int retVal = -1;
        String sql = "SELECT * FROM User WHERE username = :username";

        try(Connection con = sql2o.open()) {
            List<User> users = con.createQuery(sql)
                    .addParameter("username", username)
                    .executeAndFetch(User.class);
            if (users.size() > 0) retVal = users.get(0).getId();
        }

        return retVal;
    }

    /**
     * Insert attributes time, content, idUser and idCategory into PublicMessage.
     * @param message
     * @return
     */
    public static boolean saveMessage(PublicMessage message) {
        int rowsUpdated = 0;
        String sql = "INSERT INTO PublicMessage (time, content, idUser, idCategory) VALUES (:time, :content, :idUser, :idCategory)";
        try (Connection con = sql2o.open()) {
            con.createQuery(sql)
                    .addParameter("time", message.getTime())
                    .addParameter("content", message.getContent())
                    .addParameter("idUser", message.getIdUser())
                    .addParameter("idCategory", message.getIdCategory())
                    .executeUpdate();
            rowsUpdated = con.getResult();
        }

        if (rowsUpdated > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Insert attributes time, content, fromUser and toUser into PrivateMessage.
     * @param message
     * @return
     */
    public static boolean saveMessage(PrivateMessage message) {
        int rowsUpdated = 0;
        String sql = "INSERT INTO PrivateMessage (time, content, fromUser, toUser) VALUES (:time, :content, :fromUser, :toUser)";
        try (Connection con = sql2o.open()) {
            con.createQuery(sql)
                    .addParameter("time", message.getTime())
                    .addParameter("content", message.getContent())
                    .addParameter("fromUser", message.getFromUser())
                    .addParameter("toUser", message.getToUser())
                    .executeUpdate();
            rowsUpdated = con.getResult();
        }

        if (rowsUpdated > 0) {
            return true;
        } else {
            return false;
        }
    }

    public static List<Category> getAllCategories() {
        List<Category> retVal = new ArrayList<>();
        String sql = "SELECT * FROM Category";

        try(Connection con = sql2o.open()) {
            retVal = con.createQuery(sql)
                    .executeAndFetch(Category.class);
        }

        return retVal;
    }

    public static User getUser(int idUser) {
        User retVal = null;
        String sql = "SELECT * FROM User WHERE id = :id";

        try(Connection con = sql2o.open()) {
            List<User> users = con.createQuery(sql)
                    .addParameter("id", idUser)
                    .executeAndFetch(User.class);
            if (users.size() > 0) retVal = users.get(0);
        }

        return retVal;
    }

    public static User getUser(String username) {
        User retVal = null;
        String sql = "SELECT * FROM User WHERE username = :username";

        try(Connection con = sql2o.open()) {
            List<User> users = con.createQuery(sql)
                    .addParameter("username", username)
                    .executeAndFetch(User.class);
            if (users.size() > 0) retVal = users.get(0);
        }

        return retVal;
    }

    public static List<User> getAllUsers() {
        List<User> retVal = new ArrayList<>();
        String sql = "SELECT * FROM User";

        try(Connection con = sql2o.open()) {
            retVal = con.createQuery(sql)
                    .executeAndFetch(User.class);
        }

        return retVal;
    }

    /**
     * Get all users, which have id larger than the provided value.
     * @param id
     * @return
     */
    public static List<User> getAllUsersAfterId(int id) {
        List<User> retVal = new ArrayList<>();
        String sql = "SELECT * FROM User WHERE id > :id";

        try(Connection con = sql2o.open()) {
            retVal = con.createQuery(sql)
                    .addParameter("id", id)
                    .executeAndFetch(User.class);
        }

        return retVal;
    }


    /**
     * Get all private messages tht have id larger than the provided value.
     * @param userId
     * @param id
     * @return
     */
    public static List<PrivateMessage> getAllPrivateMessagesAfterId(int userId, int id) {
        List<PrivateMessage> retVal = new ArrayList<>();
        String sql = "SELECT * FROM PrivateMessage WHERE id > :id AND (fromUser = :fromUser OR toUser = :toUser)";

        try(Connection con = sql2o.open()) {
            retVal = con.createQuery(sql)
                    .addParameter("id", id)
                    .addParameter("fromUser", userId)
                    .addParameter("toUser", userId)
                    .executeAndFetch(PrivateMessage.class);
        }

        return retVal;
    }

    public static List<Interest> getAllInterests() {
        List<Interest> retVal = new ArrayList<>();
        String sql = "SELECT * FROM Interest";

        try(Connection con = sql2o.open()) {
            retVal = con.createQuery(sql)
                    .executeAndFetch(Interest.class);
        }

        return retVal;
    }

    public static boolean setInterest(int idUser, int idInterest) {
        int rowsUpdated = 0;
        String sql = "INSERT INTO HasInterest (idUser, idInterest) VALUES (:idUser, :idInterest)";
        try (Connection con = sql2o.open()) {
            con.createQuery(sql)
                    .addParameter("idUser", idUser)
                    .addParameter("idInterest", idInterest)
                    .executeUpdate();
            rowsUpdated = con.getResult();
        }

        if (rowsUpdated > 0) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean deleteInterest(int idUser, int idInterest) {
        int rowsUpdated = 0;
        String sql = "DELETE FROM HasInterest WHERE idUser = :idUser AND :idInterest = idInterest";
        try (Connection con = sql2o.open()) {
            con.createQuery(sql)
                    .addParameter("idUser", idUser)
                    .addParameter("idInterest", idInterest)
                    .executeUpdate();
            rowsUpdated = con.getResult();
        }

        if (rowsUpdated > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Return, whether the specific user has a specific interest.
     * @param idUser
     * @param idInterest
     * @return
     */
    public static boolean hasInterest(int idUser, int idInterest) {
        List<HasInterest> retVal = new ArrayList<>();
        String sql = "SELECT * FROM HasInterest WHERE idUser = :idUser AND :idInterest = idInterest";

        try(Connection con = sql2o.open()) {
            retVal = con.createQuery(sql)
                    .addParameter("idUser", idUser)
                    .addParameter("idInterest", idInterest)
                    .executeAndFetch(HasInterest.class);
        }

        return retVal.size() > 0;
    }

    public static boolean updateProfileImage(int idUser, byte[] image) {
        int rowsUpdated = 0;
        String sql = "UPDATE User SET image = :image WHERE id = :idUser";
        try (Connection con = sql2o.open()) {
            con.createQuery(sql)
                    .addParameter("idUser", idUser)
                    .addParameter("image", image)
                    .executeUpdate();
            rowsUpdated = con.getResult();
        }

        if (rowsUpdated > 0) {
            return true;
        } else {
            return false;
        }
    }
}
