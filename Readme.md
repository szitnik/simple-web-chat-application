# Simple Web Chat application

The application was created for a final project during the Summer school of Database design in Daegu at the Kyungpook Nation University, July 18th 2015. The students neede to implement the `DBManager` class.

The project is using Java 8, Spark Web Framework and Sql2o for the connection to MySQL database.

## Screenshots

### Login/Register screen 

![Login/Register screen](img/scr1.png "Login/Register screen")

### Public chat

![Public chat](img/scr2.png "Public chat")

### Private chat

![Private chat](img/scr3.png "Private chat")

## Installation instructions

1. Run `Chat.sql`
    * The script will create a MySQL schema, named `chat` and a MySQL user `chat` with a password `BeReSeMi.01`.    
```
mysql -u root -p < db/Chat.sql
```
2. Run `Chat_initialData.sql`    
    * The script will insert some initial data into the tables `interest` and `category`.
```
mysql -u root -p < db/Chat_initialData.sql
```
3. Build project
```
mvn clean package
```
4. Run project
```
java -jar target/chat-1.0-SNAPSHOT-jar-with-dependencies.jar
```
5. Browser automatically opens at [http://localhost:8080](http://localhost:8080), where you can start chatting :)